# Define source and destination paths
$sourcePath = "C:\Program Files (x86)\Common Files\Oracle\Java\javapath_target_*"
$destinationPath = "C:\Program Files (x86)\Common Files\Oracle\Java\hope"

# Get files matching the pattern
$filesToMove = Get-Item $sourcePath

# Move each file to the destination directory
foreach ($file in $filesToMove) {
    Move-Item -Path $file.FullName -Destination $destinationPath
}